//= ../libs/jquery/dist/jquery.min.js
//= ../libs/slick-carousel/slick/slick.min.js

$('.slider').slick({
    infinite: true,
    slidesToShow: 1,
    arrows: false,
    dots: true,
    autoplay: false,
    slidesToScroll: 1,
    customPaging: function (slider, i) {
        return '<div class="dots-block"><div class="dotscustom"></div></div>';
    },

});


$('.reviewslider').slick({
    infinite: true,
    slidesToShow: 3,
    arrows: true,
    dots: false,
    autoplay: false,
    slidesToScroll: 1,
    prevArrow: '<img src = "assets/static/left-slide-arrow.png" class="prev">',
    nextArrow: '<img src = "assets/static/right-slide-arrow.png" class="next">'
});